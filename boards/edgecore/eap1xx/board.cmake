# SPDX-License-Identifier: Apache-2.0
if(CONFIG_BOARD_EAP101_CC2652R OR CONFIG_BOARD_EAP104 OR CONFIG_BOARD_OAP101)
    # TODO: testing adapters which are 1V8 compliant
    set(OPENOCD_NRF5_INTERFACE "ftdi/jtag-lock-pick_tiny_2")
    include(${ZEPHYR_BASE}/boards/common/openocd.board.cmake)
elseif(CONFIG_BOARD_EAP101_NRF52810)
    board_runner_args(jlink "--device=nRF52810_xxAA" "--speed=4000")
    board_runner_args(pyocd "--target=nrf52810" "--frequency=4000000")

    include(${ZEPHYR_BASE}/boards/common/jlink.board.cmake)
    include(${ZEPHYR_BASE}/boards/common/pyocd.board.cmake)

    # TODO: testing adapters which are 1V8 compliant
    set(OPENOCD_NRF5_INTERFACE "ftdi/jtag-lock-pick_tiny_2")
    include(${ZEPHYR_BASE}/boards/common/openocd-nrf5.board.cmake)
elseif(CONFIG_BOARD_EAP102)
    board_runner_args(jlink "--device=nRF52840_xxAA" "--speed=4000")
    board_runner_args(pyocd "--target=nrf52840" "--frequency=4000000")

    include(${ZEPHYR_BASE}/boards/common/jlink.board.cmake)
    include(${ZEPHYR_BASE}/boards/common/pyocd.board.cmake)

    # TODO: testing adapters which are 1V8 compliant
    set(OPENOCD_NRF5_INTERFACE "ftdi/jtag-lock-pick_tiny_2")
    include(${ZEPHYR_BASE}/boards/common/openocd-nrf5.board.cmake)
endif()
