/*
 * SPDX-License-Identifier: Apache-2.0
 */

/dts-v1/;

#include <nordic/nrf52840_qiaa.dtsi>
#include "eap102-pinctrl.dtsi"

/ {
	model = "EdgeCore EAP102 with nRF52840";
	compatible = "edgecore,eap102-nrf52840";

	aliases {
		watchdog0 = &wdt0;
	};

	chosen {
		zephyr,console = &uart0;
		zephyr,shell-uart = &uart0;
		zephyr,uart-mcumgr = &uart0;
		zephyr,bt-mon-uart = &uart0;
		zephyr,bt-c2h-uart = &uart0;
		zephyr,sram = &sram0;
		zephyr,flash = &flash0;
		zephyr,code-partition = &slot0_partition;
		zephyr,settings-partition = &slot1_reserved;
		zephyr,ieee802154 = &ieee802154;
	};
};

&gpiote {
	status = "okay";
};

&gpio0 {
	status = "okay";
};

&uart0 {
	status = "okay";
	compatible = "nordic,nrf-uarte";
	pinctrl-0 = <&uart0_default>;
	pinctrl-1 = <&uart0_sleep>;
	pinctrl-names = "default", "sleep";
	current-speed = <115200>;
};

&uicr {
	gpio-as-nreset;
};

&ieee802154 {
	status = "okay";
};

&flash0 {
	partitions {
		compatible = "fixed-partitions";
		#address-cells = <1>;
		#size-cells = <1>;

		/* 48 KiB (0xc000) for MCUboot */
		boot_partition: partition@0 {
			label = "mcuboot";
			reg = <0x00000000 0x0000c000>;
		};

		/* 488 KiB (0x7a000) per slot:
		 * 472 KiB (0x76000) for image
		 *  16 KiB  (0x4000) reserved for storage partition, etc. */
		slot0_partition: partition@c000 {
			label = "image-0";
			reg = <0x0000c000 0x00076000>;
		};

		slot0_reserved: partition@82000 {
			label = "reserved-0";
			reg = <0x00082000 0x00004000>;
		};

		slot1_partition: partition@86000 {
			label = "image-1";
			reg = <0x00086000 0x00076000>;
		};

		slot1_reserved: partition@fc000 {
			label = "reserved-1";
			reg = <0x000fc000 0x00004000>;
		};
	};
};

zephyr_udc0: &usbd {
	compatible = "nordic,nrf-usbd";
	status = "okay";
};
