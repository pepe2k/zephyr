/*
 * Copyright (c) 2021 Sateesh Kotapati
 * Copyright (c) 2023 Piotr Dymacz
 *
 * SPDX-License-Identifier: Apache-2.0
 */

/delete-node/ &boot_partition;
/delete-node/ &slot0_partition;
/delete-node/ &slot1_partition;
/delete-node/ &storage_partition;

/ {
	model = "Silicon Labs EFR32BG22 Thunderboard (SLTB010A) using BRD4184B";
	compatible = "silabs,efr32bg22c224f512im40", "silabs,sltb010a",
		"silabs,efr32bg22";

	chosen {
		zephyr,settings-partition = &slot1_reserved;
	};
};

&button0 {
	gpios = <&gpiob GECKO_PIN(3) GPIO_ACTIVE_LOW>;
};

&flash0 {
	partitions {
		/* 32 KiB (0x8000) for MCUboot */
		boot_partition: partition@0 {
			label = "mcuboot";
			reg = <0x00000000 0x00008000>;
		};

		/* 240 KiB (0x3c000) per slot:
		 * 224 KiB (0x38000) for image
		 *  16 KiB  (0x4000) reserved for storage partition, etc. */
		slot0_partition: partition@8000 {
			label = "image-0";
			reg = <0x00008000 0x00038000>;
		};

		slot0_reserved: partition@40000 {
			label = "reserved-0";
			reg = <0x00040000 0x00004000>;
		};

		slot1_partition: partition@44000 {
			label = "image-1";
			reg = <0x00044000 0x00038000>;
		};

		slot1_reserved: partition@7c000 {
			label = "reserved-1";
			reg = <0x0007c000 0x00004000>;
		};
	};
};

&led0 {
	gpios = <&gpioa GECKO_PIN(4) GPIO_ACTIVE_HIGH>;
};

&sw_sensor_enable {
	enable-gpios = <&gpioc GECKO_PIN(6) GPIO_ACTIVE_HIGH>;
};

&sw_mic_enable {
	enable-gpios = <&gpioc GECKO_PIN(7) GPIO_ACTIVE_HIGH>;
};
